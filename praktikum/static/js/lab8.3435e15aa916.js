// FB initiation function
	window.fbAsyncInit = () => {
		FB.init({
			appId      : '1948879595140457',
			cookie     : true,
			xfbml      : true,
			version    : 'v2.11'
		});

		// implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
		// dan jalankanlah fungsi render di bawah, dengan parameter true jika
		// status login terkoneksi (connected)

		// Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
		// otomatis akan ditampilkan view sudah login
	};

	// Call init facebook. default dari facebook
	(function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "https://connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	 }(document, 'script', 'facebook-jssdk'));

	// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
	// merender atau membuat tampilan html untuk yang sudah login atau belum
	// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
	// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
	
	const render = loginFlag => {
		if (loginFlag) {
			// Jika yang akan dirender adalah tampilan sudah login

			// Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
			// yang menerima object user sebagai parameter.
			// Object user ini merupakan object hasil response dari pemanggilan API Facebook.
			getUserData(user => {
				// Render tampilan profil, form input post, tombol post status, dan tombol logout
				$('#lab8').html(
					'<div class="fb-profile">' +
						'<img align="left" class="fb-image-lg" src="' + user.cover.source + '" alt="cover" />' +  
						'<img align="left" class="fb-image-profile thumbnail" src="' + user.picture.data.url + '" alt="profpic" />' + 
						'<div class="fb-profile-text">' +
				
							'<h1>' + user.name + '</h1>' +
							'<p>' + user.gender + '</p>' +
				 
						'</div>' +
					'</div>' +
					'<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
					'<button class="postStatus" onclick="postStatus()"><img src="http://megacaremissions.org/wp-content/uploads/2014/07/button-share.png" width=50px height=50px></img></button>' +
					'<button class="logout" onclick="facebookLogout()"><img src="http://images-onepick-opensocial.googleusercontent.com/gadgets/proxy?container=onepick&gadget=a&rewriteMime=image%2F*&url=http%3A%2F%2Fwww.cr-cath.pvt.k12.ia.us%2Flasalle%2FResources%2FRev%2520War%2520Websites%2FJosh%2520Michael%2520Zack%2520Rev%2520War%2FZack%2520Rev%2520War%2Fimages%2FBack%2520Button.jpg" height=50px width=50px> </img></button>'
				);
		
		$('#lab8').append(
			'<h1 id="posts"> FEED </h1>'
		);
				// Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
				// dengan memanggil method getUserFeed yang kalian implementasi sendiri.
				// Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
				// ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
				getUserFeed(feed => {
					feed.data.map(value => {
						// Render feed, kustomisasi sesuai kebutuhan.
						if (value.message && value.story) {
							$('#lab8').append(
				'<button id="masuk"><img src="https://lh3.googleusercontent.com/mfYavj0l7EVvnjDvMzRHwDXi4rnieicARfErsMOCgFoUySUAUIhN_tJAunPkCA7s7VrnQ7XDA_FrM2PR88Rn=w1920-h921" height=25px width=25px></img></button>' +
								'<div class="feed">' +
									'<p>' + value.message + '</p>' +
									'<p>' + value.story + '</p>' +
					'</div> <br>'
							);
						} else if (value.message) {
							$('#lab8').append(
				'<button id="masuk"><img src="https://lh3.googleusercontent.com/mfYavj0l7EVvnjDvMzRHwDXi4rnieicARfErsMOCgFoUySUAUIhN_tJAunPkCA7s7VrnQ7XDA_FrM2PR88Rn=w1920-h921" height=25px width=25px></img></button>' +
								'<div class="feed">' +
									'<p>' + value.message + '</p>' +
								'</div> <br>'
							);
						} else if (value.story) {
							$('#lab8').append(
				'<button id="masuk"><img src="https://lh3.googleusercontent.com/mfYavj0l7EVvnjDvMzRHwDXi4rnieicARfErsMOCgFoUySUAUIhN_tJAunPkCA7s7VrnQ7XDA_FrM2PR88Rn=w1920-h921" height=25px width=25px></img></button>' +
								'<div class="feed">' +
									'<p>' + value.story + '</p>' + 
								'</div> <br>'
							);
						}
					});
				});
			});
		} else {
			// Tampilan ketika belum login
			$('#lab8').html(
		'<button id="login-button"  onclick="facebookLogin()"><center><img id="hehe" src="http://mauisold.com/images/Logos/FB-f-Logo__white.png" height=50px width=50px><b>Login With Facebook</b></img></button></center>');
		}
	};

	const facebookLogin = () => {
		 FB.login(function(response){
		 console.log(response);
	 render(true);
		 }, {scope:'public_profile,user_posts,publish_actions'})
	};

	const facebookLogout = () => {
		 FB.getLoginStatus(function(response) {
			 if (response.status === 'connected') {
				 FB.logout();
		 render(false);
				}
		 });
	};

	// TODO: Lengkapi Method Ini
	// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
	// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
	// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
	// meneruskan response yang didapat ke fungsi callback tersebut
	// Apakah yang dimaksud dengan fungsi callback?
	const getUserData = (fun) => {
	 FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					FB.api('/me?fields=id,name,cover,picture,about,email,gender', 'GET', function(response){
						console.log(response);
			if(response && !response.error){
				fun(response);
			} else{
				alert("Something is missing");
			}
					});
				}
		});
	};

	const getUserFeed = (fun) => {
		FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					FB.api('/me/posts', 'GET', function(response){
						console.log(response);
			if(response && !response.error){
				fun(response);
			} else{
				alert("Something is missing");
			}
					});
				}
		});
	};

	const postFeed = (message) => {
		FB.api('/me/feed', 'POST', {message:message});
	render(true);
	};

	const postStatus = () => {
		const message = $('#postInput').val();
		postFeed(message);
	};
